!/bin/bash

SANDBOX=/var/www/bintangpelajar/sandbox.bintangpelajar.net
chmod -R a+rwx $SANDBOX/simteg_application/logs
chmod -R a+rwx $SANDBOX/simteg_application/cache
chmod -R a+rwx $SANDBOX/assets/fileuser
chmod -R a+rwx $SANDBOX/assets/frontend
chmod -R a+rwx $SANDBOX/assets/upload_files
chmod -R a+rwx $SANDBOX/system_32/core
chmod -R a+rwx $SANDBOX/system_32/database
